trigger ShiftTrigger on Shift__c (after insert,after update,after delete,after undelete) {
	if(Trigger.isAfter)
    {
        ShiftTriggerHandler.updateShiftCount(Trigger.new,Trigger.oldMap);
    }
}