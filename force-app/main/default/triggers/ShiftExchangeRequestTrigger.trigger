trigger ShiftExchangeRequestTrigger on Shift_Exchange_Request__c (after update) {
    
        ShiftExchangeRequestTriggerHandler.exchangeShift(Trigger.new,Trigger.oldMap);

}