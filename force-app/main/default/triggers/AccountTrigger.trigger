trigger AccountTrigger on Account (before delete,after insert,after update,after delete,after undelete) {
 // Apex code
 // 
    if(Trigger.isBefore && Trigger.isUpdate){
        // code to execute on before update
    }
    
    if(Trigger.isAfter){
        // Code will execute on after insert , after update , after delete and after undelete
    }
    
    // Apex code - get executed for all mentioned events
    // 
    System.debug(Trigger.newMap);  // This contains all new version of records Map<Id,Account>
    
    // Before Insert - Trigger.New - list of records
    // After Insert - Trigger.New - list of records
    // BEfore Insert - Trigger.old - null
    // After Insert - Trigger.old - null
    // Before Insert - Trigger.newMap -- null 
    // After Insert -- Trigger.newMap --New version of records present in form of map <Id,Account>
 }