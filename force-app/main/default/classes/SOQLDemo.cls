public class SOQLDemo {
    public static void queryParentfromChild(){
        List<Shift__c> shiftList = [SELECT Id,Name,Employee__c,
                                    Employee__r.Name,
                                    Employee__r.Account.Name,
                                    Employee__r.Account.Owner.Name
                                    FROM Shift__c LIMIT 1];
        for(Shift__c shift : shiftList){
            System.debug('Shift Name '+shift.Name);
            System.debug('Employee Name '+shift.Employee__r.Name);
            System.debug('Employee Department Name '+shift.Employee__r.Account.Name);
            System.debug('Employee Department Owner Name '+shift.Employee__r.Account.Owner.Name);
        }
        
    }
    
    public static void queryChildfromParent(){
        List<Contact> contactList = [SELECT Id,Name,
                                     (SELECT Id,Name,Employee__c,Employee__r.Name FROM Shifts__r)
                                     FROM Contact WHERE Account.Name = 'MTX Group'];
        for(Contact employee : contactList){
            List<Shift__c> shiftList = new List<Shift__c>();
            shiftList = employee.Shifts__r;
            system.debug('>>>>'+employee.Name+'>>>>>'+shiftList);
        }
        
        
    }
}