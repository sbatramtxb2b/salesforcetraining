public class ShiftExchangeRequestTriggerHandler {
	public static void exchangeShift(List<Shift_Exchange_Request__c> newList,Map<Id,Shift_Exchange_Request__c> oldMap)
    {
        //variables
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id,Id> userIdToContactIdMap = new Map<Id,Id>();
        Set<Date> dateSet = new Set<Date>();
        Map<String,Shift__c> contactIdDateWithShiftMap = new Map<String,Shift__c>();
        Id requestedById, requestedWithId;
        Shift__c requestedByShift,requestedWithShift;
        String tempVarForShiftContact;
        List<Shift__c> shiftListToUpdate = new List<Shift__c>();
        
        
        //adding all the dates and contact id's in a set so that they can provide filter for 
        //querying shift
        for(Shift_Exchange_Request__c shiftExchange : newList)
        {
            if(shiftExchange.Exchange_Shift_With__c != null &&
            shiftExchange.Requested_By__c != null &&
            shiftExchange.Shift_Date__c != null &&
            shiftExchange.Status__c == 'Approved' &&
            shiftExchange.Status__c!=oldMap.get(shiftExchange.Id).Status__c)
            {
                //adding to sets
                dateSet.add(shiftExchange.Shift_Date__c);
                contactIdSet.add(shiftExchange.Exchange_Shift_With__c); 
                contactIdSet.add(shiftExchange.Requested_By__c);
            }
        }
        
        //querying all the shifts with the filters and adding the shift based on shift date and contact
        //shift date + contact id is used to provide uniqueness to Map Keys
        for(Shift__c shift : [SELECT Id, Assigned_Shift__c,
                              Shift_Date__c,Employee__c 
                              FROM Shift__c
                             WHERE Shift_Date__c IN :dateSet
                              AND Employee__c IN :contactIdSet]){
                        
            contactIdDateWithShiftMap.put(shift.Shift_Date__c+'-'+shift.Employee__c,shift);                      
        }

        for(Shift_Exchange_Request__c shiftExchange : newList)
        {
            if(shiftExchange.Exchange_Shift_With__c != null &&
            shiftExchange.Requested_By__c != null &&
            shiftExchange.Shift_Date__c != null &&
            shiftExchange.Status__c == 'Approved' &&
            shiftExchange.Status__c!=oldMap.get(shiftExchange.Id).Status__c)
            {
                //getting requestedby and exchange with contact Id
                requestedById = shiftExchange.Requested_By__c;
                requestedWithId = shiftExchange.Exchange_Shift_With__c;
                
                //getting the shift by using requestedby and exchange with contact Id and shift date
                requestedByShift = contactIdDateWithShiftMap.get(shiftExchange.Shift_Date__c+'-'+requestedById);
                requestedWithShift  = contactIdDateWithShiftMap.get(shiftExchange.Shift_Date__c+'-'+requestedWithId);

                //swapping the contact ids of both the shifts
                if(requestedByShift != null && requestedWithShift != null){
                    //taking a temporary variable to store Requested By Shift's contact 
                    tempVarForShiftContact = requestedByShift.Employee__c;
                    //updating (requested by) shift with (exchange with) contact id
                    requestedByShift.Employee__c = requestedWithShift.Employee__c;
                    //updating (exchange with) shift by (requested by) contact id using the temporary variable
                    requestedWithShift.Employee__c = tempVarForShiftContact;

                    shiftListToUpdate.add(requestedByShift);
                    shiftListToUpdate.add(requestedWithShift);
                } 
            }
        }

        if(!shiftListToUpdate.isEmpty()){
            update shiftListToUpdate; // once your shifts are udpated , Whift trigger will also get called to update the count.
        }

    }
}