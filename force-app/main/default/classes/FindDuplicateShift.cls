public class FindDuplicateShift {
	
    public static void markDuplicateShifts(){
        Set<String> shiftSet = new Set<String>();
        List<Shift__c> shiftList = new List<Shift__c>();
        for(Shift__c shift : [SELECT Id,Shift_Date__c,Employee__c,Employee__r.Name
                              FROM Shift__c]){
                                  String key = String.valueOf(shift.Shift_Date__c) + shift.Employee__c;
                                  if(!shiftSet.contains(key)){
                                      shiftSet.add(key);
                                  }else{
                                      shift.Duplicate_Shift__c = true;
                                      shiftList.add(shift);
                                  }
                                  
                              }
        for(Shift__c duplidateShift : shiftList){
            System.debug('>>>>'+duplidateShift.Shift_Date__c+'>>>>>'+duplidateShift.Employee__r.Name);
        }
        if(shiftList.size() > 0){
            update shiftList;
        }
    }
}