public with sharing class ShiftController {
    public ShiftController() {

    }

    @AuraEnabled
    public static List<ShiftWrapper> findShifts(String contactId) { 
        List<ShiftWrapper> shiftList = new List<ShiftWrapper>();
       for(Shift__c shift : [SELECT Id,Assigned_Shift__c,
                            Shift_Date__c,Employee__c,Employee__r.Name 
                            FROM Shift__c
                            WHERE Employee__c=:contactId
                            ORDER BY Shift_Date__c]){
                                Boolean editable = true;
                                if(shift.Shift_Date__c <  Date.today().addDays(2)){
                                    editable = false;
                                }
                                shiftList.add(new ShiftWrapper(shift,editable));
                            }
        return shiftList; 
    } 
  
    public class ShiftWrapper{
        @AuraEnabled
        public Shift__c shiftRecord{get;set;}
        @AuraEnabled
        public Boolean isEditable{get;set;}
        public ShiftWrapper(Shift__c shift,Boolean isEditable){
            this.shiftRecord = shift;
            this.isEditable = isEditable;
        }
    }
}