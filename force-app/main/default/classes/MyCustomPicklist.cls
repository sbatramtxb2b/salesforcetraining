global class MyCustomPicklist extends VisualEditor.DynamicPickList{
    
    global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('Auto', 'auto');
        return defaultValue;
    }
    global override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DataRow value1 = new VisualEditor.DataRow('Auto', 'auto');
        VisualEditor.DataRow value2 = new VisualEditor.DataRow('Cursive', 'cursive');
        VisualEditor.DataRow value3 = new VisualEditor.DataRow('Monospace', 'monospace');
        VisualEditor.DataRow value4 = new VisualEditor.DataRow('Sans Serif', 'sans-serif');
        VisualEditor.DynamicPickListRows  myValues = new VisualEditor.DynamicPickListRows();
        myValues.addRow(value1);
        myValues.addRow(value2);
        myValues.addRow(value3);
        myValues.addRow(value4);
        return myValues;
    }
}