public class SetupShifts {
    // Method to delete all shift records
    public static void deleteAllshifts(){
        List<Shift__c> shiftList = [SELECT Id FROM Shift__c];
        delete shiftList;
    }
    
    // Method to insert single shift record
    public static void createSingleShift(){
        Shift__c myShift = new Shift__c();
        myShift.Shift_Date__c = Date.today(); // Date Class is predefined class
        myShift.Assigned_Shift__c = 'Morning';
        myShift.Employee__c = '0032w00000DDJRqAAP';
        insert myShift;
        system.debug('>>>>>>>Shift Id'+myShift.Id);
        system.debug('>>>>>>>Shift Record'+myShift);
    }
    
    // Method to insert bulk shift records
    public static void createShifts(){
        List<String> availableShift = new List<String>{'Morning','Afternoon','Evening'};
        Integer listSize = availableShift.size() - 1;
        Set<Id> employeeSet = new Set<Id>();
        List<Shift__c> shiftList = new List<Shift__c>();
        for(Contact con : [SELECT Id,Name
                           FROM Contact WHERE Account.Name = 'MTX Group']){
                               employeeSet.add(con.Id);
        }
        // Create Shift for Employee
        for(Integer count = 0;count<=30;count++){
            Date tempDate = Date.today().addDays(count);
            DateTime dt = DateTime.newInstance(tempDate.year(),tempDate.month(),tempDate.day());
			String dayOfWeek = dt.format('EEEE');
            if(!(dayOfWeek.equalsIgnoreCase('Saturday') || dayOfWeek.equalsIgnoreCase('Sunday'))){ // Do not create shift for Weekend
                for(Id employeeID : employeeSet){
             		Shift__c myShift = new Shift__c();
        	 		myShift.Shift_Date__c = tempDate; // Date Class is predefined class
                    Integer randomNumber = Integer.valueof((Math.random() * listSize)); // Using Math class to generate random number
        	 		myShift.Assigned_Shift__c = availableShift.get(randomNumber);
        	 		myShift.Employee__c = employeeID;
                  //  insert myShift; // DML limit of 150 
                    shiftList.add(myShift); // Insert 10K records in one go
        		}
            }
        }
        if(shiftList.size() > 0){
            insert shiftList; // 46 new records
        }  
    }

}