public class ShiftTriggerHandler {
    public static void updateShiftCount(List<Shift__c> newList,Map<Id,Shift__c> oldMap)
    {
        Map<Id,Map<String,Integer>> contactIdToShiftTypeCountMap=new Map<Id,Map<String,Integer>>();
        Set<Id> contactIdSet=new Set<Id>();
        Integer countMorning = 0, countAfterNoon = 0, countEvening = 0;
        if(newList!=null) // insert , update , undelete
        {
            for(Shift__c shift : newList)
            {	// oldmap == null -- Insert
                if(oldMap==null || shift.Employee__c!=oldMap.get(shift.Id).Employee__c || shift.Assigned_Shift__c!=oldMap.get(shift.Id).Assigned_Shift__c)
                {
                    if(shift.Employee__c!=null)
                    {
                        contactIdSet.add(shift.Employee__c); //contacts where count needs to be updated                    
                    }
                    if(oldMap!=null && oldMap.get(shift.Id).Employee__c!=null)
                    {
                        contactIdSet.add(oldMap.get(shift.Id).Employee__c); //contacts where count needs to be updated   
                    }
                }
            }
        }
        
        else if(newList==null && oldMap!=null) //only for delete
        {
            for(String shiftId : oldMap.keySet())
            {
                if(oldMap.get(shiftId).Employee__c!=null)
                {
                    contactIdSet.add(oldMap.get(shiftId).Employee__c);
                }
            }
        }

        List<Contact> contactList = [SELECT Id, (SELECT Id,Assigned_Shift__c FROM Shifts__r) 
                               FROM Contact 
                               WHERE Id IN :contactIdSet];

        for(Contact con : contactList){
            countMorning = 0;
            countAfterNoon = 0;
            countEvening = 0;
            
            if(con.Shifts__r != null){
                for(Shift__c shift : con.Shifts__r){
                    if(shift.Assigned_Shift__c == 'Morning'){
                        countMorning++;
                    } else if(shift.Assigned_Shift__c == 'Afternoon'){
                        countAfterNoon++;
                    } if(shift.Assigned_Shift__c == 'Evening'){
                        countEvening++;
                    }
                }
            }
            Map<String,Integer> shiftTypeToCountMap = new Map<String,Integer>{
                                                        'Morning' => countMorning,
                                                        'Afternoon' => countAfterNoon,
                                                        'Evening' => countEvening
                                                    };
            contactIdToShiftTypeCountMap.put(con.Id,shiftTypeToCountMap);
        }

        
        List<Contact> contactUpdateList=new List<Contact>();
        for(Id conId : contactIdToShiftTypeCountMap.keySet())
        {
            Contact con=new Contact();
            con.id=conId;
            con.Total_Morning_Shifts__c=contactIdToShiftTypeCountMap.get(conId).get('Morning');
            con.Total_Afternoon_Shifts__c=contactIdToShiftTypeCountMap.get(conId).get('Afternoon');
            con.Total_Evening_Shifts__c=contactIdToShiftTypeCountMap.get(conId).get('Evening');
            contactUpdateList.add(con);
        }

        if(!contactUpdateList.isEmpty())
        {
            update contactUpdateList;
        }
    }
}