public class DeleteDuplicateShiftScheduler implements Schedulable {
    
    public void execute(SchedulableContext sc){
        List<Shift__c> shiftList = new List<Shift__c>();
        shiftList = [SELECT Id FROM Shift__c
                     WHERE Duplicate_Shift__c = true];
        if(shiftList.size() > 0){
            delete shiftList;
        }
    }
}