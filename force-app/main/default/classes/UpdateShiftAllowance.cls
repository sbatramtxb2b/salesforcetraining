public class UpdateShiftAllowance {
	
    public static void updateShift(){
        // Get shift Allowance from Shift Allowance Object
        List<String> selectedShift = new List<String>{'Afternoon','Evening'};
        Map<String,Decimal> shiftToAllowanceMap = new Map<String,Decimal>();
        List<Shift__c> shiftUpdateList = new List<Shift__c>(); 
        for(Shift_Allowance__c allowance : [SELECT Name,Allowance_in_USD__c
                                            FROM Shift_Allowance__c]){
                                                shiftToAllowanceMap.put(allowance.Name,allowance.Allowance_in_USD__c);
                                            }
         System.debug('>>>>'+shiftToAllowanceMap);
        for(Shift__c shift : [SELECT Id,Assigned_Shift__c
                              FROM Shift__c WHERE Assigned_Shift__c IN:selectedShift
                              AND Shift_Date__c > :Date.today().addDays(2)]){ // Use of : to get values from variable
                                  Shift__c myShift = new Shift__c();
                                  myShift.Id = shift.Id;
                                  myShift.Shift_Allowance__c = shiftToAllowanceMap.get(shift.Assigned_Shift__c);
                                  shiftUpdateList.add(myShift);
                              }
        if(shiftUpdateList.size() > 0){
            System.debug(shiftUpdateList);
            System.debug('>>>>'+shiftUpdateList.size());
            update shiftUpdateList;
        }
    }
}