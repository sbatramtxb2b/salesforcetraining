//old one
public class ShiftExchangeRequestTriggerHandlerOld {
	public static void exchangeShift(List<Shift_Exchange_Request__c> newList,Map<Id,Shift_Exchange_Request__c> oldMap)
    {
        Set<Id> userIdSet = new Set<Id>();
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id,Id> userIdToContactIdMap = new Map<Id,Id>();
        Map<Id,Map<Date,Shift__c>> contactIdToDateWiseShiftMap = new Map<Id,Map<Date,Shift__c>>();
        
        for(Shift_Exchange_Request__c shiftExchange : newList)
        {
            if(shiftExchange.Exchange_Shift_With__c != null &&
            shiftExchange.Shift_Date__c != null &&
            shiftExchange.Status__c == 'Approved' &&
            shiftExchange.Status__c!=oldMap.get(shiftExchange.Id).Status__c)
            {
                userIdSet.add(shiftExchange.CreatedById);
                contactIdSet.add(shiftExchange.Exchange_Shift_With__c);
            }
        }

        List<Contact> contactList = [SELECT Id, 
                                    (SELECT Id,Assigned_Shift__c,
                                    Shift_Date__c,Employee__c 
                                    FROM Shifts__r) 
                                    FROM Contact 
                                    WHERE Id IN :contactIdSet];

        for(Contact con : contactList){
            if(con.Shifts__r != null){
                Map<Date,Shift__c> dateToShiftMap = new Map<Date,Shift__c>();
                for(Shift__c shift : con.Shifts__r){
                    dateToShiftMap.put(shift.Shift_Date__c,shift);
                }
                contactIdToDateWiseShiftMap.put(con.Id,dateToShiftMap);
            }  
        }

        List<Contact> contactFromUserList = [SELECT Id,Employee__c,
                                            (SELECT Id,Assigned_Shift__c,
                                            Shift_Date__c,Employee__c 
                                            FROM Shifts__r) 
                                            FROM Contact 
                                            WHERE Employee__c IN :userIdSet];

        for(Contact con : contactFromUserList){
            if(con.Shifts__r != null){
                Map<Date,Shift__c> dateToShiftMap = new Map<Date,Shift__c>();
                for(Shift__c shift : con.Shifts__r){
                    dateToShiftMap.put(shift.Shift_Date__c,shift);
                }
                contactIdToDateWiseShiftMap.put(con.Id,dateToShiftMap);
            }    
            userIdToContactIdMap.put(con.Employee__c,con.Id);
        }

        Id requestedById, requestedWithId;
        Shift__c requestedByShift,requestedWithShift;
        String tempVarForShiftContact;
        List<Shift__c> shiftListToUpdate = new List<Shift__c>();

        for(Shift_Exchange_Request__c shiftExchange : newList)
        {
            if(shiftExchange.Exchange_Shift_With__c != null &&
            shiftExchange.Shift_Date__c != null &&
            shiftExchange.Status__c == 'Approved' &&
            shiftExchange.Status__c!=oldMap.get(shiftExchange.Id).Status__c)
            {
                requestedById = userIdToContactIdMap.get(shiftExchange.CreatedById);
                requestedWithId = shiftExchange.Exchange_Shift_With__c;
                
                requestedByShift = contactIdToDateWiseShiftMap.get(requestedById).get(shiftExchange.Shift_Date__c);
                requestedWithShift  = contactIdToDateWiseShiftMap.get(requestedWithId).get(shiftExchange.Shift_Date__c);

                if(requestedByShift != null && requestedWithShift != null){
                    tempVarForShiftContact = requestedByShift.Employee__c;
                    requestedByShift.Employee__c = requestedWithShift.Employee__c;
                    requestedWithShift.Employee__c = tempVarForShiftContact;

                    shiftListToUpdate.add(requestedByShift);
                    shiftListToUpdate.add(requestedWithShift);
                } 
            }
        }

        if(!shiftListToUpdate.isEmpty()){
            update shiftListToUpdate;
        }

    }
}