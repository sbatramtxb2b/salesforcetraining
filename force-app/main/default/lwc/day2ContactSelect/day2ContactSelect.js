import { LightningElement,api,track,wire } from 'lwc';
import getRelatedContact from '@salesforce/apex/AccountController.getRelatedContact';
import {fireEvent} from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
export default class Day2ContactSelect extends LightningElement {
    @api accountid = '';
    selectedContact = 'None';
    @track contacts=[];
    @wire(getRelatedContact,{accountId : '$accountid'})
   getAllContacts({error, data}){
    if(data){
        this.contacts = [];
        const temp = { label: 'None', value: 'None' };
        this.contacts = [ ...this.contacts, temp ];
        for(var item of data) {
            var con = { label: item.Name, value: item.Id };
            this.contacts = [ ...this.contacts, con ];
        }
    }
   }
   @wire(CurrentPageReference)pageRef;
   handleContactChange(event){
        this.selectedContact = event.detail.value;
        this.selectedContactName = this.contacts.find(contact => contact.value === this.selectedContact);
        if(this.selectedContact !== 'None'){
            const selectedEvent = new CustomEvent('selected', { detail: { contactname: this.selectedContactName.label}});
            this.dispatchEvent(selectedEvent);
            fireEvent(this.pageRef,'pubsubselected',this.selectedContactName.value);
        }
        
   }
}