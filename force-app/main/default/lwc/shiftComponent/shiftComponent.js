import {
    LightningElement,
    wire,
    api
} from 'lwc';
import {
    NavigationMixin
} from "lightning/navigation";
import findShifts from '@salesforce/apex/ShiftController.findShifts';

export default class ShiftComponent extends NavigationMixin(LightningElement) {
    @api recordId;
    shifts = [];

    connectedCallback() {
        this.shifts = [];
        this.handleLoad();
    }

    handleLoad() {
        findShifts({
                contactId: this.recordId
            })
            .then(result => {
                this.shifts = result;
            })
            .catch(error => {
                JSON.stringify('errror>>' + error);
            });
    }

    handleClick(event) {
        let recId = event.currentTarget.dataset.id;
        console.log('rec>>' + recId);

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recId,
                objectApiName: 'Shift__c',
                actionName: 'edit'
            },
        });
    }

    renderedCallback() {
        this.handleLoad();
    }

}