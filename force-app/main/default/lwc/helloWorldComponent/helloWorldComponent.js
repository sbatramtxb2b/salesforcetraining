import { LightningElement } from 'lwc';

export default class HelloWorldComponent extends LightningElement {
   message = "Welcome";

   handleMessagechange(event){
        this.message = event.target.value;
   }
}