import { LightningElement,wire,track } from 'lwc';
import {registerListener,unregisterAllListeners} from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
export default class Day2eventDemo extends LightningElement {
    @track selectedContact = '';
    @wire(CurrentPageReference)pageRef;

    contactSelectHandler(data){
        this.selectedContact = data;
    }

    connectedCallback(){
        registerListener('pubsubselected',this.contactSelectHandler,this);
    }

    disconnectedCallback(){
        unregisterAllListeners(this);
    }
}