import { LightningElement,track,wire } from 'lwc';
import getAccountList from '@salesforce/apex/AccountController.getAccountList';
export default class Day2AccountSelect extends LightningElement {
    value = 'None';
    @track options=[];
    @track showContact = false;
    @wire(getAccountList)
    wiredAccounts({ error, data }) {
        if(data){
            const temp = { label: 'None', value: 'None' };
            this.options = [ ...this.options, temp ];
            for(var item of data) {
                var acc = { label: item.Name, value: item.Id };
                this.options = [ ...this.options, acc ];
            }
        }
        if(error){
            // handle error;
        }
    }

    handleChange(event){
       this.value = event.detail.value;
       if(this.value !== 'None'){
           this.showContact = true;
       }
    }

    contactSelected(event) {
        const contact = event.detail;
        alert('Selected Contact is '+contact.contactname);
    }

}