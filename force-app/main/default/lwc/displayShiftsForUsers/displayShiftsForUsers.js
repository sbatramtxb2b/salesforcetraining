import { LightningElement,track,wire,api } from 'lwc';
import getAccountList from '@salesforce/apex/AccountController.getAccountList';
import getRelatedContact from '@salesforce/apex/AccountController.getRelatedContact';
export default class DisplayShiftsForUsers extends LightningElement {
    value = 'None';
   @track options=[];
   @track contacts=[];
   @track allContacts;
   @api font;
   @api color;
    selectedContact = 'None';
    @wire(getAccountList)
    wiredAccounts({ error, data }) {
        if(data){
            const temp = { label: 'None', value: 'None' };
            this.options = [ ...this.options, temp ];
            for(var item of data) {
                var acc = { label: item.Name, value: item.Id };
                this.options = [ ...this.options, acc ];
            }
        }
        if(error){
            // handle error;
        }
    }

    handleChange(event){
       this.value = event.detail.value;
    }

   // @wire(getRelatedContact,{accountId : '$value'}) allContacts;
   @wire(getRelatedContact,{accountId : '$value'})
   getAllContacts({error, data}){
    if(data){
        this.contacts = [];
        const temp = { label: 'None', value: 'None' };
        this.contacts = [ ...this.contacts, temp ];
        for(var item of data) {
            var con = { label: item.Name, value: item.Id };
            this.contacts = [ ...this.contacts, con ];
        }
    }
   }

   handleContactChange(event){
        this.selectedContact = event.detail.value;
   }

}