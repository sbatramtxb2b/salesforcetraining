import { LightningElement,api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class ShiftDetails extends LightningElement {
    @api recordId;

    handleSuccess(event) {
        this.showToast();
    }


    showToast() {
        const myevent = new ShowToastEvent({
            title: 'Success',
            message: 'Record Updated Successfully',
            variant : 'success',
        });
        this.dispatchEvent(myevent);
    }
}